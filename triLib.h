/**
 * @file 
 * @author Franck
 * @version 1.1
 * @brief header de la librairie de tri
 * @date 25-09-2019
 */

/**
 * Declaration de la fonction de tri a bulle
 * @param tableau a trier
 * @param taille du tableau
 */
void triabulle(float [], int);
/**
 * Declaration de la fonction de tri par insertion
 * @param tableau a trier
 * @param taille du tableau
 */
void triparinsertion(float [], int);
/**
 * Declaration de la fonction de tri par selection
 * @param tableau a trier
 * @param taille du tableau
 */
void triparselection(float [], int);
/**
 * Declaration du benchmark de la fonction de tri a bulle
 * @param tableau a trier
 * @param taille du tableau
 * @return durée du tri
 */
float bench_triabulle(float [], int);
/**
 * Declaration du benchmark de la fonction de tri par insertion
 * @param tableau a trier
 * @param taille du tableau
 * @return durée du tri
 */
float bench_triparinsertion(float [], int);
/**
 * Declaration du benchmark de la fonction de tri par selection
 * @param tableau a trier
 * @param taille du tableau
 * @return durée du tri
 */
float bench_triparselection(float [], int);
/**
 * Declaration de la fonction de création de CSV des benchmarks
 * @param nom du fichier
 * @param tableau de résultats des benchmarks
 */
void create_csv(char *,float [][3]);
