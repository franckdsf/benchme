/**
 * @file 
 * @author Franck
 * @version 1.1
 * @brief fichier de la fonction de tri par selection
 * @date 25-09-2019
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Tri d'un tableau avec une fonction de tri par selection
 * @param tab tableau a trier
 * @param sizeoftab taille du tableau a trier
 */
void triparselection(float *tab, int sizeoftab){
    int min_index = 0; //on déclare les variables
    float _temp;
    
    /*on parcourt le tableau à la recherche de la plus petite 
    valeur(1) et on l'a met au début(2), puis on reprend la recherche
     à partir de cet index(3)*/
    for(int j = 0; j < sizeoftab-1; j++){ 
        
        //Etape 3
        for(int i = j; i < sizeoftab; i++){
            //Etape 1
            if(tab[i] < tab[min_index]){ 
                min_index = i;
            }
        }    
        
        //Etape 2
        _temp = tab[j];
        tab[j] = tab[min_index];
        tab[min_index] = _temp;
        min_index = j+1;
    }
}

