## Application permettant de trier des tableaux de 3 façons différentes

3 façons différentes de trier utilisés ici : 

- _Tri à bulle :_ Le système compare les 2 premières valeurs, prend la plus petite et la met à la position 0. Puis il compare la 2ème et 3ème valeur, et met la plus petite à la position 1...<br>

- _Tri par sélection :_ Le système parcourt tout le tableau et cherche la plus petite valeur. Il l'a met ensuite à la position 0. Il reparcourt tout le tableau en partant de la position 1 et cherche la plus petite valeur pour la mettre à la position 1...<br>

- _Tri par insertion :_ Le système parcourt tout le tableau pour trouver la plus petite valeur X, puis décale toutes les valeurs plus grandes que X pour la positionner à la place vacante.

## Usage des commandes

Une fois le programme lancé, il suffit simplement d'entrer 3 tailles de tableau.<br>

Le programme va générer des nombres aléatoires entre 0 et 10⁶ pour remplir les tableaux, et va ensuite les trier.<br>

Il va également être capable de calculer la moyenne du temps d'éxecution de chaque fonction de tri (à bulle, sélection ou insertion) basée sur 3 tris.<br>

Cette moyenne de temps d'éxecution va automatiquement être exporté dans un fichier .csv où il sera possible de consulter le temps d'éxecution des fonctions pour les 3 tailles.

## Résultats attendus

- L'application doit être capable de _générer des nombres aléatoires entre 0 et 10⁶_ pour remplir les tableaux.
- Il doit être capable de _retourner les tableaux triés du plus petit au plus grand_.
- Il doit _retourner la moyenne du temps d'éxecution de chaque fonction_ basé sur 3 tests différents.
- _Un fichier .csv doit être créé automatiquement contenant la moyenne du temps d'éxecution de chaque fonction_ en fonction des 3 tailles de tableau entrées au début.

## Evolutions à venir 


Implémentation de futurs fonction de tri comme le _QuickSort_ ou _TimSort_.<br>

Optimisation au maximum de la mise en forme du fichier .csv généré automatiquement.<br>
